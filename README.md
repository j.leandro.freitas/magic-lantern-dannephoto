# Magic Lantern Dannephoto

Magic Lantern
Magic Lantern (ML) is a software enhancement that offers increased functionality to the excellent Canon DSLR cameras.

It's an open framework, licensed under GPL, for developing extensions to the official firmware.

Magic Lantern is not